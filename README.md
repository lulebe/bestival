# Bestival  - Festivalplaylists

Bestival is a mobile app that reads concert and festival events from your calendar to create Spotify playlists. This helps with finding out which gigs you want to see!

There are 3 submodules:
- iOS Repo
- android Repo
- nodejs server Repo


This Project is licensed under MIT License.
